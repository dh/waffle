
The Waffle bugfix release 1.7.2 is now available.

What is new in this release:
  - all: use format(gnu_printf), enable in mingw
  - meson: don't run TLS checks on mingw
  - wgl: remove unused dummy wgl_error.[ch]

The source is available at:

  https://waffle.freedesktop.org/files/release/waffle-1.7.2/waffle-1.7.2.tar.xz

You can also get the current source directly from the git
repository. See https://waffle.freedesktop.org for details.

----------------------------------------------------------------

Changes since 1.7.1:

Emil Velikov (4):
      meson: don't run TLS checks on mingw
      wgl: remove unused dummy wgl_error.[ch]
      all: use format(gnu_printf), enable in mingw
      waffle: Bump version to 1.7.2

